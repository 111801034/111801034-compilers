structure PP : sig
   val compile : Tiger.ast -> string end = 
   struct

   val cyan  = "\027[96m";
   val pink  = "\027[95m";
   val white = "\027[37m";


   fun compile (e0) = 
    let  fun indent 0 = ("")
          | indent i = (" " ^ indent(i-1))

          fun opname Tiger.PLUS = "+"
            | opname Tiger.MINUS = "-"
            | opname Tiger.MUL = "*"
            | opname Tiger.DIVIDE = "/"
            | opname Tiger.EQUAL = "="
            | opname Tiger.NEQUAL = "!="
            | opname Tiger.SLESSER = "<"
            | opname Tiger.LTE = "<="
            | opname Tiger.SGREATER = ">"
            | opname Tiger.GTE = ">="
            | opname Tiger.AND = "&"
            | opname Tiger.OR = "|"

          fun dolist d f [a] sep = (f(a,d))
            | dolist d f (a::r) sep = (f(a,d) ^ pink ^ sep ^ white ^ (dolist d f r sep))
            | dolist d f nil sep = ("")  

         fun lvalue(Tiger.lvalue_ID(s),d) = (s)                                                         
            | lvalue(Tiger.lvalue_record(v,s),d) = (indent d ^ lvalue(v,d) ^ "." ^ s)
            | lvalue(Tiger.lvalue_array(v,e),d) = (indent d ^ lvalue(v,d) ^ "[" ^ exp(e,0) ^ "]")



        and exp(Tiger.NULL, d) = (cyan ^ "nil" ^ white)
        | exp(Tiger.exp_Int i, d) = (indent d ^ pink ^ (Int.toString (i)) ^ white)
        | exp(Tiger.exp_Str(s),d) = (indent d ^ s)
        | exp(Tiger.exp_lvalue(l_value), d) = (lvalue(l_value,d+1))
        | exp(Tiger.functionCall{function_id,function_exps},d) =
		        (indent d ^ function_id ^ pink ^ "(" ^ "\n" ^ white ^ (dolist (d+1) exp function_exps ",") ^ "\n" ^ indent d ^ pink ^ ")" ^ white)
        | exp(Tiger.mathTree_exp{left_exp,operation,right_exp},d) =
		        (indent d ^ exp(left_exp,0) ^ indent 1 ^ pink ^ (opname operation) ^ white ^ indent 1 ^ exp(right_exp,0))
        | exp(Tiger.minus_exp{operation,exp_body},d) =
		        (indent d ^ pink ^ (opname operation) ^ white ^ indent 1 ^ exp(exp_body,0))
        | exp(Tiger.record{record_datatype,record_list},d) =
            let fun f((name,e),d) = 
                (indent d ^ name ^ pink ^ " = " ^ white ^ exp(e,d))
            in (indent d ^ record_datatype) 
                ^ "{" ^ (dolist d f record_list ",") ^ "}" 
            end
        | exp(Tiger.temp_exp {t_exps}, d) = (indent d ^ pink ^ "(" ^ white ^ "\n" ^ (dolist (d+2) exp (t_exps) "; \n") ^ "\n" ^ indent d ^ pink ^ ")" ^ white)
        | exp(Tiger.assign{var_name=v,value=e},d) = 
            (lvalue(v,d) ^ pink ^ " := " ^ white ^ exp(e,0))
        | exp(Tiger.if_then_statement{if_condition,if_then},d) =
            (indent d ^ cyan ^ "if " ^ white ^ exp(if_condition,0) ^ cyan ^ " then" ^ white ^ "\n" ^ exp(if_then,d+1))
        | exp(Tiger.if_then_else_statement{if_condition,if_then,if_else},d) =
            (indent d ^ cyan ^ "if " ^ white ^ exp(if_condition,0) ^ cyan ^ " then" ^ white ^ "\n" ^ exp(if_then,d+1) ^ "\n" ^ indent (d+1) ^ cyan ^ "else" ^ white ^ "\n"
            ^ exp(if_else,d+2))
        | exp(Tiger.while_loop{while_condition,while_body},d) =
            (indent d ^ cyan ^ "while " ^ white ^ exp(while_condition,0) ^ cyan ^ " do" ^ white ^ "\n" ^ exp(while_body,d+1))
        | exp(Tiger.for_loop{for_loop_start,for_loop_id=v,for_loop_end,for_loop_body},d) =
            (indent d ^ cyan ^ "for " ^ white ^ v ^ " := " 
            ^ exp(for_loop_start,0) ^ cyan ^ " to " ^ white ^ exp(for_loop_end,0) ^ cyan ^ " do" ^ white ^ "\n"
            ^ exp(for_loop_body,d+1))
        | exp(Tiger.break ,d) = (indent d ^ cyan ^ "break" ^ white)
        | exp(Tiger.let_exp{let_declaration,let_body_exps},d) =
            (indent d ^ cyan ^ "let " ^ white ^ "\n" ^ decs(let_declaration,d+1) ^ "\n" ^ cyan ^ "in " ^ white ^ "\n" ^ exps(let_body_exps,d+1) ^ "\n" ^ cyan ^ "end " ^ white ^ "\n")        
        | exp(Tiger.array{array_datatype,array_init,array_length},d) =
	        ((array_datatype) ^ pink ^ "[" ^ white ^
            exp(array_length,0) ^ pink ^ "]" ^ white ^ cyan ^ " of " ^ white ^ exp(array_init,0))

        and exps(l, d) = (dolist d exp l ";")


        and dec(Tiger.FunctionDec_optional{fun_id,fun_tyfields,fun_body}, d) = 
            (indent d ^ cyan ^ " function " ^ white ^ (fun_id) ^ " (" ^ function_helper(Tiger.Record_type fun_tyfields,0) ^ ")" ^ " = " ^ "\n" ^ exp(fun_body,d+1))
        | dec(Tiger.FunctionDec{fun_id,fun_tyfields,fun_type_id,fun_body}, d) = 
            (indent d ^ cyan ^ " function " ^ white ^ (fun_id) ^ " (" ^ function_helper(Tiger.Record_type fun_tyfields,0) ^ ")" ^ " : " ^ (fun_type_id) ^ " = " ^ "\n" ^ exp(fun_body,d+1))
        | dec(Tiger.VariableDec(var_dec),d) = 
            (indent d ^ vardec(var_dec,d))
        | dec(Tiger.TypeDec {name,ty=t}, d) = 
            (indent d ^ cyan ^ " type " ^ white ^ (name) ^ " = " ^ ty(t,d))

        and decs(l, d) = (dolist d dec l "\n")



        and vardec(Tiger.Var{Var_id, Var_type_id, Var_value},d) = 
            (indent d ^ cyan ^ "var " ^ white ^ (Var_id) ^ " : " ^ (Var_type_id) ^ " := " ^ exp(Var_value,0))
        | vardec(Tiger.Var_optional{Var_id,Var_value},d) = 
            (indent d ^ cyan ^ "var " ^ white ^ (Var_id) ^ " := " ^ exp(Var_value,0))
    
        and ty(Tiger.ty_name(s), d) = (indent d ^ (s))
        | ty(Tiger.record_type l, d) = 
            (indent d ^ "{ " ^ (dolist d typefield l ",") ^ " }")
        | ty(Tiger.array_type(s),d) = (indent d ^ cyan ^ "array of " ^ white ^ (s))

        and function_helper(Tiger.Record_type l,d) = 
            (indent d ^ (dolist d typefield l ","))

        and typefield(Tiger.Tyf {name,typ}, d) = 
			(indent d ^ (name) ^ " : " ^ (typ))
  

    in  case e0 of 
            Tiger.ast_exp e             => 	(exp(e,0) ^ "")
            | Tiger.ast_dec_list d_list => 	(decs(d_list,0) ^ "")
    end





   (* something here *)
end
