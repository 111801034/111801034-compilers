## Version Tracking
- [17-02-2021] Inital Version
- [23-02-2021] Hello World compiler
- [01-03-2021] Integer division and Parentesis, added to reverse-polish compiler
- [08-03-2021] mips is implemented based on their respective concrete syntax
- [09-03-2021] AST of tiger is implemented based on their respective concrete syntax
- [17-03-2021] Added lexer, parser of tiger language
- [21-03-2021] Added the printAst, which prints the generated abstract syntax tree of some tiger code
- [02-04-2021] Done Pretty print 
- [30-05-2021] Done Translation from Tiger AST to IR AST 



## Git Commit Hash
- LAB3:
    - cb2913e6d105f705636346d0641f12f0eba8e425(older)[COMMAND: `make && ./tc testcasefilepath`]
    - 1e6748b0a89478d9daae566768db99c2283302db(newer)[COMMAND: `make && ./tc --ast testcasefilepath`]
- LAB4 
    - 89506b1718f966ca91cdc10ae880f6975d478d31(older)[COMMAND: `make && ./tc testcasefilepath`]
    - 1e6748b0a89478d9daae566768db99c2283302db(newer)[COMMAND: `make && ./tc --pp testcasefilepath`]
- LAB5
    - Done only Tree.sml and Tree.sig (first part of lab5 assignment)
- LAB6
    - 3043dcdd7776ff92a6f3071db8662b98a9981166[COMMAND: `make && ./tc --ir testcasefilepath`]
