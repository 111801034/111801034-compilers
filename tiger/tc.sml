
structure TC =
struct


structure ASTLrVals = ASTLrValsFun(structure Token = LrParser.Token)
structure ASTLex    = ASTLexFun(structure Tokens = ASTLrVals.Tokens)
structure ASTParser = Join( structure ParserData = ASTLrVals.ParserData
			     structure Lex        = ASTLex
			     structure LrParser   = LrParser
			   )

fun makeASTLexer strm = ASTParser.makeLexer (fn n => TextIO.inputN(strm,n))
val makeFileLexer      = makeASTLexer o TextIO.openIn

val thisLexer = case CommandLine.arguments() of
		    []  		 => makeASTLexer TextIO.stdIn
		 |  [x] 		 => makeFileLexer x
		 |  ["--ast", x] => makeFileLexer x
		 |  [x, "--ast"] => makeFileLexer x
		 |  ["--pp", x]  => makeFileLexer x
		 |  [x, "--pp"]  => makeFileLexer x
		 |  ["--ir", x]  => makeFileLexer x
		 |  [x, "--ir"]  => makeFileLexer x
		 |  _   => (TextIO.output(TextIO.stdErr, "usage: tc file"); OS.Process.exit OS.Process.failure)

fun print_error (s,i:int,_) = TextIO.output(TextIO.stdErr,
					    "Error, line " ^ (Int.toString i) ^ ", " ^ s ^ "\n")

val (program,_) = ASTParser.parse (0,thisLexer,print_error,()) (* parsing *)


val PP_Printing  = PP.compile(program)
val IR_ast  = ast_translate.translate(program)

val output = case CommandLine.arguments() of
		       []           => TextIO.output(TextIO.stdOut, PP_Printing)
			|  [x]          => TextIO.output(TextIO.stdOut, PP_Printing)
			|  ["--ast", x] => PrintAbsyn.print (TextIO.stdOut, program)
			|  [x, "--ast"] => PrintAbsyn.print (TextIO.stdOut, program)
		    |  ["--pp",x]   => TextIO.output(TextIO.stdOut, PP_Printing)
			|  [x, "--pp"]  => TextIO.output(TextIO.stdOut, PP_Printing)
			|  ["--ir", x]  => PrintIR.print (TextIO.stdOut, Tree.ir_stmt(IR_ast))
		 	|  [x, "--ir"]  => PrintIR.print (TextIO.stdOut, Tree.ir_stmt(IR_ast))
		    |  _   => (TextIO.output(TextIO.stdErr, "usage: tc file"); OS.Process.exit OS.Process.failure)

end

