(* Section-1 *)

type lineNo            = int
type pos = lineNo
val lineRef : pos ref = ref 0

type svalue        = Tokens.svalue
type ('a,'b) token = ('a,'b) Tokens.token
type lexresult     = (svalue,pos) token

fun eof   ()      = Tokens.EOF (!lineRef,!lineRef)

fun charsToInt m (x :: xs) = charsToInt (10 * m + ord x - ord #"0") xs
  | charsToInt m []        = m

fun toSigned (#"-" :: xs) = ~ (charsToInt 0 xs)
  | toSigned (#"~" :: xs) = ~ (charsToInt 0 xs)
  | toSigned (#"+" :: xs) =   charsToInt 0 xs
  | toSigned xs           =   charsToInt 0 xs

val toInt        = toSigned o String.explode



(* Section-2 *)

%%
%header (functor ASTLexFun(structure Tokens : AST_TOKENS));
ws = [\ \t];
digits = [0-9]+;
single_line_comments = \/\/(.*);
multi_line_comments = "/*"([^*]|\*+[^*/])*\*+"/";
letters = [A-Za-z0-9_]+;
word = [\"][^\"]*[\"];


%%

" "                      => ( lex ());
\t                       => ( lex ());
{ws}+                    => ( lex ());
{single_line_comments}   => ( lex() );
{multi_line_comments}    => ( lex() );

\n           => ( lex ());
\r           => ( lex ());
\n\r         => ( lex ());
\r\n         => ( lex ());
\n({ws}*\n) *=> ( lex() );
           
"+"           => ( Tokens.PLUS  (!lineRef,!lineRef) );
"-"           => ( Tokens.MINUS  (!lineRef,!lineRef) );
"*"           => ( Tokens.MUL (!lineRef,!lineRef) );
"/"           => ( Tokens.DIVIDE (!lineRef,!lineRef) );
"("           => ( Tokens.lOpenBrack  (!lineRef,!lineRef) );
")"           => ( Tokens.rOpenBrack  (!lineRef,!lineRef) );
"{"           => ( Tokens.lFlowerBrack  (!lineRef,!lineRef) );
"}"           => ( Tokens.rFlowerBrack  (!lineRef,!lineRef) );
"["           => ( Tokens.lCloseBrack  (!lineRef,!lineRef) );
"]"           => ( Tokens.rCloseBrack  (!lineRef,!lineRef) );
","           => ( Tokens.comma  (!lineRef,!lineRef) );
":"           => ( Tokens.colon  (!lineRef,!lineRef) );
";"           => ( Tokens.semicolon  (!lineRef,!lineRef) );
"."           => ( Tokens.DOT  (!lineRef,!lineRef) );
"="           => ( Tokens.EQUAL  (!lineRef,!lineRef) );
"<>"          => ( Tokens.NEQUAL  (!lineRef,!lineRef) );
"<"           => ( Tokens.SLESSER  (!lineRef,!lineRef) );
"<="          => ( Tokens.LTE  (!lineRef,!lineRef) );
">"           => ( Tokens.SGREATER  (!lineRef,!lineRef) );
">="          => ( Tokens.GTE  (!lineRef,!lineRef) );
"&"           => ( Tokens.AND  (!lineRef,!lineRef) );
"|"           => ( Tokens.OR  (!lineRef,!lineRef) );
":="          => ( Tokens.ASSIGN  (!lineRef,!lineRef) );
array         => ( Tokens.ARRAY (!lineRef,!lineRef) );
if            => ( Tokens.IF (!lineRef,!lineRef) );
then          => ( Tokens.THEN (!lineRef,!lineRef) );
else          => ( Tokens.ELSE (!lineRef,!lineRef) );
while         => ( Tokens.WHILE (!lineRef,!lineRef) );
for           => ( Tokens.FOR (!lineRef,!lineRef) );
"do"          => ( Tokens.DO (!lineRef,!lineRef) );
to            => ( Tokens.TO (!lineRef,!lineRef) );
let           => ( Tokens.LET (!lineRef,!lineRef) );
in            => ( Tokens.IN (!lineRef,!lineRef) );
end           => ( Tokens.END (!lineRef,!lineRef) );
of            => ( Tokens.OF (!lineRef,!lineRef) );
break         => ( Tokens.BREAK (!lineRef,!lineRef) );
nil           => ( Tokens.NIL (!lineRef,!lineRef) );
function      => ( Tokens.FUNCTION (!lineRef,!lineRef) );
var           => ( Tokens.VAR (!lineRef,!lineRef) );
"type"        => ( Tokens.TYPE (!lineRef,!lineRef) );


{digits}     => ( Tokens.INT (toInt yytext, !lineRef, !lineRef) );
{letters}    => ( Tokens.ID (yytext, !lineRef, !lineRef) );
{word}       => ( Tokens.STR (yytext, !lineRef, !lineRef) );