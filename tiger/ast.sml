structure Tiger = struct

datatype ast = ast_exp of exp
	     | ast_dec_list of dec list


and exp = NULL
	| exp_Int of int
	| exp_Str of string
	| array of { array_datatype : string,
		     array_length : exp,
		     array_init : exp}
	| record of { record_datatype : string,
		      record_list : (string * exp) list}
	| exp_lvalue of lvalue
	| functionCall of { function_id : string,
			    function_exps : exp list}
	| mathTree_exp of { left_exp  : exp,
			    operation : Op,
			    right_exp : exp}
	| minus_exp of { operation : Op,
	                 exp_body : exp}
	| temp_exp of {t_exps: exp list}
	| assign of { var_name : lvalue,
		      value : exp}
	| if_then_statement of { if_condition : exp,
			    if_then : exp}
	| if_then_else_statement of { if_condition : exp,
			    if_then : exp,
			    if_else : exp}
	| while_loop of { while_condition : exp,
			  while_body : exp}
	| for_loop of { for_loop_id : string,
			for_loop_start : exp,
			for_loop_end : exp,
			for_loop_body : exp}
	| break
	| let_exp of { let_declaration : dec list,
		       let_body_exps : exp list}
		

and lvalue = lvalue_ID of string
	   | lvalue_record of (lvalue * string)
	   | lvalue_array of (lvalue * exp)



and dec =  TypeDec of {name: string, ty: ty}
	|  VariableDec of vardec
	|  FunctionDec_optional of { fun_id : string,
			    fun_tyfields : typefield list,
			    fun_body : exp}
	|  FunctionDec of { fun_id : string,
			    fun_tyfields : typefield list,
			    fun_type_id : string,
			    fun_body : exp}

and decs = Dec of dec list

and exps = Exp of exp list


and vardec = Var of { Var_id : string,
		      Var_type_id : string,
		      Var_value : exp}
			| Var_optional of { Var_id : string,
		      Var_value : exp}

and ty = ty_name of string
       | record_type of typefield list
       | array_type of string

and function_helper = Record_type of typefield list

and typefield = Tyf of {name: string, typ:string}


and tyfields = TyFs of typefield list


and Op = PLUS | MINUS | MUL | DIVIDE | EQUAL | NEQUAL | SGREATER | SLESSER | GTE | LTE | AND | OR

end
