structure PrintAbsyn : sig val print : TextIO.outstream * Tiger.ast -> unit end =
struct


fun print (outstream, e0) =
 let fun say s =  TextIO.output(outstream,s)
  fun sayln s= (say s; say "\n") 

  fun indent 0 = ()
    | indent i = (say " "; indent(i-1))

  fun opname Tiger.PLUS = "PLUS"
    | opname Tiger.MINUS = "MINUS"
    | opname Tiger.MUL = "MUL"
    | opname Tiger.DIVIDE = "DIVIDE"
    | opname Tiger.EQUAL = "EQUAL"
    | opname Tiger.NEQUAL = "NEQUAL"
    | opname Tiger.SLESSER = "SLESSER"
    | opname Tiger.LTE = "LTE"
    | opname Tiger.SGREATER = "SGREATER"
    | opname Tiger.GTE = "GTE"
	| opname Tiger.AND = "AND"
	| opname Tiger.OR = "OR"

  fun dolist d f [a] = (sayln ""; f(a,d+1))
    | dolist d f (a::r) = (sayln ""; f(a,d+1); say ","; dolist d f r)
    | dolist d f nil = ()


  fun lvalue(Tiger.lvalue_ID(s),d) = (indent d; say "SimpleVar("; 
			         say(s); say ")")                                                         
    | lvalue(Tiger.lvalue_record(v,s),d) = (indent d; sayln "FieldVar(";
				  lvalue(v,d+1); sayln ",";
				  indent(d+1); say(s); say ")")
    | lvalue(Tiger.lvalue_array(v,e),d) = (indent d; sayln "SubscriptVar(";
				      lvalue(v,d+1); sayln ",";
				      exp(e,d+1); say ")")
  and exp(Tiger.NULL, d) = (indent d; say "NilExp")
    | exp(Tiger.exp_Int i, d) = (indent d; say "IntExp("; say(Int.toString i);
			    say ")")
    | exp(Tiger.exp_Str(s),d) = (indent d; say "StringExp(\"";
				 say s; say "\")")
	| exp(Tiger.exp_lvalue(l_value), d) = (indent d; say "exp_lvalue("; lvalue(l_value,d+1); say ")" )
    | exp(Tiger.functionCall{function_id,function_exps},d) =
		        (indent d; say "functionCallExp("; say(function_id);
			 say ",["; dolist d exp function_exps; say "])")
    | exp(Tiger.mathTree_exp{left_exp,operation,right_exp},d) =
		(indent d; say "OpExp("; say(opname operation); sayln ",";
		 exp(left_exp,d+1); sayln ","; exp(right_exp,d+1); say ")")
    | exp(Tiger.minus_exp{operation,exp_body},d) =
		(indent d; say "OpExp("; say(opname operation); sayln ",";
		 exp(exp_body,d+1); say ")")
    | exp(Tiger.record{record_datatype,record_list},d) =
	    let fun f((name,e),d) = 
			(indent d; say "("; say(name);
			 sayln ","; exp(e,d+1);
			 say ")")
	     in indent d; say "RecordExp("; say(record_datatype); 
	        sayln ",["; dolist d f record_list; say "])" 
	    end
    | exp(Tiger.temp_exp {t_exps}, d) = (indent d; say "SeqExp["; dolist d exp (t_exps); 
			    say "]")
    | exp(Tiger.assign{var_name=v,value=e},d) = 
		(indent d; sayln "AssignExp("; lvalue(v,d+1); sayln ",";
		 exp(e,d+1); say ")")
    | exp(Tiger.if_then_statement{if_condition,if_then},d) =
		(indent d; sayln "If_then_statement("; exp(if_condition,d+1); sayln ",";
		 exp(if_then,d+1);
		 say ")")
    | exp(Tiger.if_then_else_statement{if_condition,if_then,if_else},d) =
		(indent d; sayln "If_then_else_statement("; exp(if_condition,d+1); sayln ",";
		 exp(if_then,d+1);
         (sayln ","; exp(if_else,d+1));
		 say ")")
    | exp(Tiger.while_loop{while_condition,while_body},d) =
		(indent d; sayln "WhileExp("; exp(while_condition,d+1); sayln ",";
		 exp(while_body,d+1); say ")")
    | exp(Tiger.for_loop{for_loop_start,for_loop_id=v,for_loop_end,for_loop_body},d) =
		(indent d; sayln "ForExp(";
		 say(v); say ",";
		 exp(for_loop_start,d+1); sayln ","; exp(for_loop_end,d+1); sayln ",";
		 exp(for_loop_body,d+1); say ")")
    | exp(Tiger.break ,d) = (indent d; say "BreakExp")
    | exp(Tiger.let_exp{let_declaration,let_body_exps},d) =
		(indent d; say "LetExp([";
		 decs(let_declaration,d+1); sayln "],"; exps(let_body_exps,d+1); say")") 
    | exp(Tiger.array{array_datatype,array_init,array_length},d) =
	        (indent d; say "ArrayExp("; say(array_datatype); sayln ",";
		 exp(array_length,d+1); sayln ","; exp(array_init,d+1); say ")")

	and exps(l, d) = (indent d; say "exps("; dolist d exp l;
        say ")")


	and dec(Tiger.FunctionDec_optional{fun_id,fun_tyfields,fun_body}, d) = 
		(indent d; say "funDec_optional("; say(fun_id); say ",";
		ty(Tiger.record_type fun_tyfields,d+1); say ","; exp(fun_body,d+1); say")")
	| dec(Tiger.FunctionDec{fun_id,fun_tyfields,fun_type_id,fun_body}, d) = 
		(indent d; say "funDec("; say(fun_id); say ",";
		ty(Tiger.record_type fun_tyfields,d+1); say ","; say(fun_type_id); say ","; exp(fun_body,d+1); say")")
    | dec(Tiger.VariableDec(var_dec),d) = 
		(indent d; say "VariableDec("; vardec(var_dec,d); say ")")
    | dec(Tiger.TypeDec {name,ty=t}, d) = 
	  (indent d; say "TypeDec["; say"("; say(name); say ",";ty(t,d+1); say ")"; say "]")

  and decs(l, d) = (indent d; say "Dec("; dolist d dec l;
        say ")")

  and vardec(Tiger.Var{Var_id, Var_type_id, Var_value},d) = 
		(indent d; say "VarDec("; say(Var_id); say ",";
		(say "type("; say(Var_type_id); say ")");
		sayln ","; exp(Var_value,d+1); say ")")
	| vardec(Tiger.Var_optional{Var_id,Var_value},d) = 
		(indent d; say "VarDec("; say(Var_id);
		sayln ","; exp(Var_value,d+1); say ")")
   
  and ty(Tiger.ty_name(s), d) = (indent d; say "NameTy("; say(s);
			      say ")")
	| ty(Tiger.record_type l, d) = 
		(indent d; say "RecordTy("; dolist d typefield l; say ")")
    | ty(Tiger.array_type(s),d) = (indent d; say "ArrayTy("; say(s);
			      say ")")

  and typefield(Tiger.Tyf {name,typ}, d) = 
			(indent d; say "("; say (name);
		         say ",";
			 say (typ); say ")")
            

 in  case e0 of 
 		  Tiger.ast_exp e           => 	(exp(e,0); sayln "")
		| Tiger.ast_dec_list d_list => 	(decs(d_list,0); sayln "");
		TextIO.flushOut outstream
end

end
