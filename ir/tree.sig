signature TREE =
  sig

     datatype binop = PLUS | MINUS | MUL | DIV 
                | AND | OR | LSHIFT | RSHIFT | ARSHIFT | XOR

     datatype relop = EQ | NE | LT | GT | LE | GE 
	        | ULT | ULE | UGT | UGE

     datatype ir = ir_exp of exp
              |   ir_stmt of stmt
     
     and exp = CONST of int
                   | NAME  of Temp.label
		       (* tiger level : functions, destinations for conditionals etc *)
                       (* processor : assembly language address *)
		   | TEMP  of Temp.temp
		       (* tiger level : variables unbounded number *)
                       (* processor level : processor registers    *)
		   | BINOP of binop * exp * exp
		   | MEM   of exp   (* processor level : an address's content *)
                   | CALL  of exp * exp list
                                     (* func (arg1, arg2 ....) *)
		   | ESEQ of stmt * exp
     and stmt = MOVE  of exp * exp
              | EXP   of exp
              | JUMP  of exp * Temp.label list
	      | CJUMP of relop * exp * exp * Temp.label * Temp.label
	      (* CJUMP rop e1 e2 l1 l2  = if e1 rop e2 then goto l1 else goto l2 *)
	      | SEQ of stmt * stmt
              | LABEL of Temp.label


end

structure Tree : TREE = 
struct
  type label=Temp.label
  type size = int

datatype ir = ir_exp of exp
          |   ir_stmt of stmt

     and stmt = SEQ of stmt * stmt
             | LABEL of label
             | JUMP of exp * label list
             | CJUMP of relop * exp * exp * label * label
	     | MOVE of exp * exp
             | EXP of exp

     and exp = BINOP of binop * exp * exp
             | MEM of exp
             | TEMP of Temp.temp
             | ESEQ of stmt * exp
             | NAME of label
             | CONST of int
	     | CALL of exp * exp list

      and binop = PLUS | MINUS | MUL | DIV 
                | AND | OR | LSHIFT | RSHIFT | ARSHIFT | XOR

      and relop = EQ | NE | LT | GT | LE | GE 
	        | ULT | ULE | UGT | UGE

end
