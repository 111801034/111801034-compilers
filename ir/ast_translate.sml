structure ast_translate : 
	sig val translate : Tiger.ast -> Tree.stmt end =
struct
  
fun translate (e0) =
	let 
  		fun binop Tiger.PLUS = Tree.PLUS
    	  | binop Tiger.MINUS = Tree.MINUS
		  | binop Tiger.MUL = Tree.MUL
		  | binop Tiger.DIVIDE = Tree.DIV
          | binop Tiger.AND = Tree.AND
		  | binop Tiger.OR = Tree.OR
        
        fun relop Tiger.EQUAL = Tree.EQ
		  | relop Tiger.NEQUAL = Tree.NE
		  | relop Tiger.SGREATER = Tree.GT
		  | relop Tiger.SLESSER = Tree.LT
		  | relop Tiger.GTE = Tree.GE
		  | relop Tiger.LTE = Tree.LE

  		fun exp(Tiger.NULL) = Tree.EXP( Tree.CONST(0) )
		  | exp(Tiger.exp_Int e) = Tree.EXP( Tree.CONST(e) )
		  | exp(Tiger.mathTree_exp{left_exp,operation,right_exp}) = 
		  					(let
		  						val a = case exp(left_exp) of Tree.EXP (e1) => (e1)
								val b = case exp(right_exp) of Tree.EXP (e2) => (e2)
								val l1 = Temp.newlabel()
								val l2 = Temp.newlabel()
		  					in	
								case operation of Tiger.EQUAL    =>  Tree.CJUMP(relop operation, a, b, l1, l2)
								  				| Tiger.NEQUAL   =>  Tree.CJUMP(relop operation, a, b, l1, l2)
												| Tiger.SGREATER =>  Tree.CJUMP(relop operation, a, b, l1, l2)
												| Tiger.SLESSER  =>  Tree.CJUMP(relop operation, a, b, l1, l2)
												| Tiger.GTE      =>  Tree.CJUMP(relop operation, a, b, l1, l2)
												| Tiger.LTE      =>  Tree.CJUMP(relop operation, a, b, l1, l2)
												| _              =>  Tree.EXP( Tree.BINOP(binop operation, a, b ) )
							end )
		  | exp(Tiger.exp_lvalue(l_value)) = lvalue(l_value)
		  | exp(Tiger.if_then_statement{if_condition,if_then}) = ( let fun ifThen_helper []        = (Tree.EXP(Tree.CONST(0)))
																		  |  ifThen_helper [s]     = (s)
																		  |  ifThen_helper (s::sl) = Tree.SEQ( s, ifThen_helper (sl) )
																		val l1 = Temp.newlabel()
																		val l2 = Temp.newlabel()
																		val r = Temp.newtemp()
																		val a = Tree.ESEQ( exp(if_condition), Tree.CONST(1))
																	 in
																	 	Tree.EXP( Tree.ESEQ( ifThen_helper[  Tree.MOVE(Tree.TEMP r, Tree.CONST(1)),
																		 									 Tree.CJUMP(Tree.EQ, a, Tree.CONST(1), l1, l2), 
																		                                     Tree.LABEL l1, exp(if_then),
																											 Tree.LABEL l2, Tree.EXP(Tree.CONST(0)), Tree.MOVE(Tree.TEMP r, Tree.CONST(0))] , Tree.CONST(0) ) )
																	 end )
		  | exp(Tiger.if_then_else_statement{if_condition,if_then,if_else}) = ( let fun ifThen_helper []     = (Tree.EXP(Tree.CONST(0)))
																					|  ifThen_helper [s]     = (s)
																					|  ifThen_helper (s::sl) = Tree.SEQ( s, ifThen_helper (sl) )
																					val l1 = Temp.newlabel()
																					val l2 = Temp.newlabel()
																					val r = Temp.newtemp()
																					val a = Tree.ESEQ( exp(if_condition), Tree.CONST(1))
																				in
																					Tree.EXP( Tree.ESEQ( ifThen_helper[ Tree.MOVE(Tree.TEMP r, Tree.CONST(1)),
																														Tree.CJUMP(Tree.EQ, a, Tree.CONST(1), l1, l2), 
																														Tree.LABEL l1, exp(if_then),
																														Tree.LABEL l2, exp(if_else), Tree.MOVE(Tree.TEMP r, Tree.CONST(1))] , Tree.CONST(0) ) )
																				end )
		  | exp(Tiger.while_loop{while_condition,while_body}) = ( let fun while_helper []         = (Tree.EXP(Tree.CONST(0)))
																		  |  while_helper [s]     = (s)
																		  |  while_helper (s::sl) = Tree.SEQ( s, while_helper (sl) )
																		val l1 = Temp.newlabel()
																		val l2 = Temp.newlabel()
																		val a = Tree.ESEQ( exp(while_condition), Tree.CONST(1))
																	 in
																	 	Tree.EXP( Tree.ESEQ( while_helper[Tree.CJUMP(Tree.EQ, a, Tree.CONST(1), l1, l2), 
																		                                     Tree.LABEL l1, exp(while_body), Tree.CJUMP(Tree.EQ, a, Tree.CONST(1), l1, l2),
																											 Tree.LABEL l2, Tree.EXP(Tree.CONST(0))] , Tree.CONST(0) ) )
																	 end )
		  | exp(Tiger.for_loop{for_loop_start,for_loop_id=v,for_loop_end,for_loop_body}) = ( let fun for_helper []    = (Tree.EXP(Tree.CONST(0)))
																									|  for_helper [s]     = (s)
																									|  for_helper (s::sl) = Tree.SEQ( s, for_helper (sl) )
																								val l1 = Temp.newlabel()
																								val l2 = Temp.newlabel()
																								val a = case exp(for_loop_start) of Tree.EXP (e1) => (e1)
		  											 											val b = case exp(for_loop_end) of Tree.EXP (e2) => (e2)
																								in
																								Tree.EXP( Tree.ESEQ( for_helper[Tree.CJUMP(Tree.LE, a, b, l1, l2), 
																																		Tree.LABEL l1, exp(for_loop_body), Tree.CJUMP(Tree.LE, a, b, l1, l2),
																																		Tree.LABEL l2, Tree.EXP(Tree.CONST(0))] , Tree.CONST(0) ) )
																								end )

		  | exp(Tiger.assign{var_name=v,value=e}) = (let val a = case lvalue(v) of Tree.EXP (e1) => (e1)
		  											 val b = case exp(e) of Tree.EXP (e2) => (e2)
												  in 
		  										     Tree.MOVE( a, b )
												 end )  

		  | exp(Tiger.temp_exp {t_exps}) = ( let fun temp_exps_helper []        = Tree.EXP(Tree.CONST(0))
													|  temp_exps_helper [s]     = exp(s)
													|  temp_exps_helper (s::sl) = ( Tree.SEQ( exp(s), temp_exps_helper (sl) ) )
											 in 
											 	(temp_exps_helper t_exps)
											 end )

		and lvalue(Tiger.lvalue_ID(s)) = ( let
												val a = Temp.newtemp()
												val b = Temp.newlabel()
										   in
										   		Tree.EXP( Tree.MEM( Tree.BINOP( Tree.PLUS, Tree.TEMP a, Tree.CONST(b)) ) )
											end )
	in  
		case e0 of Tiger.ast_exp (e) =>  exp(e)
	end
end