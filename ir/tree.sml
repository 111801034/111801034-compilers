signature TEMP =
  sig
     type label = int
     type temp = int
     val newlabel : unit -> label  (* generate a new label *)
     val newtemp  : unit -> temp
  end



structure Temp :> TEMP = struct


   type label = int (* 2⁶⁴ = ∞ many variables *)
   type temp = int

   (* you can use IntInf.int *)

   val nextLabel = ref 1
   val nextTemp  = ref 1

   fun newlabel _ = let val t_Label = !nextLabel in nextLabel := t_Label+1; t_Label end
   fun newtemp  _ = let val t_Temp = !nextTemp in nextTemp := t_Temp+1; t_Temp end


end