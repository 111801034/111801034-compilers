structure PrintIR : 
     sig val print : TextIO.outstream * Tree.ir -> unit end =
struct

fun print (outstream, s0) =
 let fun say s =  TextIO.output(outstream,s)
  fun sayln s= (say s; say "\n") 

  fun indent 0 = ()
    | indent i = (say " "; indent(i-1))

  fun stmt(Tree.SEQ(a,b),d) =
          (indent d; sayln "SEQ("; stmt(a,d+1); sayln ","; stmt(b,d+1); say ")")
    | stmt(Tree.LABEL lab, d) = (indent d; say "LABEL "; say (Int.toString lab))
    | stmt(Tree.JUMP (e,_), d) =  (indent d; sayln "JUMP("; exp(e,d+1); say ")")
    | stmt(Tree.CJUMP(r,a,b,t,f),d) = (indent d; say "CJUMP(";
				relop r; sayln ",";
				exp(a,d+1); sayln ","; exp(b,d+1); sayln ",";
				indent(d+1); say(Int.toString t); 
				say ","; say (Int.toString f); say ")")
    | stmt(Tree.MOVE(a,b),d) = (indent d; sayln "MOVE("; exp(a,d+1); sayln ",";
			    exp(b,d+1); say ")")
    | stmt(Tree.EXP e, d) = (indent d; sayln "EXP("; exp(e,d+1); say ")")

  and exp(Tree.BINOP(p,a,b),d) = (indent d; say "BINOP("; binop p; sayln ",";
			       exp(a,d+1); sayln ","; exp(b,d+1); say ")")
    | exp(Tree.MEM(m),d) = (indent d; sayln "MEM("; exp(m,d+1); say ")")
    | exp(Tree.TEMP t, d) = (indent d; say "TEMP t"; say(Int.toString t))
    | exp(Tree.ESEQ(s,e),d) = (indent d; sayln "ESEQ("; stmt(s,d+1); sayln ",";
			  exp(e,d+1); say ")")
    | exp(Tree.NAME n, d) = (indent d; say "NAME "; say (Int.toString n))
    | exp(Tree.CONST i, d) = (indent d; say "CONST "; say(Int.toString i))

  and binop Tree.PLUS = say "PLUS"
    | binop Tree.MINUS = say "MINUS"
    | binop Tree.MUL = say "MUL"
    | binop Tree.DIV = say "DIV"
    | binop Tree.AND = say "AND"
    | binop Tree.OR = say "OR"
    | binop Tree.LSHIFT = say "LSHIFT"
    | binop Tree.RSHIFT = say "RSHIFT"
    | binop Tree.ARSHIFT = say "ARSHIFT"
    | binop Tree.XOR = say "XOR"

  and relop Tree.EQ = say "EQ"
    | relop Tree.NE = say "NE"
    | relop Tree.LT = say "LT"
    | relop Tree.GT = say "GT"
    | relop Tree.LE = say "LE"
    | relop Tree.GE = say "GE"
    | relop Tree.ULT = say "ULT"
    | relop Tree.ULE = say "ULE"
    | relop Tree.UGT = say "UGT"
    | relop Tree.UGE = say "UGE"
 in  
      case s0 of Tree.ir_exp(e) => (exp(e, 0); sayln "")
               | Tree.ir_stmt(s) => (stmt(s,0); sayln "");
      TextIO.flushOut outstream
  end

end

