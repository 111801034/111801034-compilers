
%.lex.sml: %.lex
	mllex $<

%.grm.sml: %.grm
	mlyacc $<


all: tc


tc: tiger/tc.sml tiger/ast.sml tiger/tiger.grm.sml tiger/tiger.lex.sml tiger/mips.sml
	mlton -output tc tiger/tc.mlb


hwc: src/hello_world.sml
	mlton -output hello_world.exec src/hello_world.sml


clean: 
	rm -rf tiger/*.lex.sml tiger/*.grm.sml tiger/*.grm.desc tiger/*.grm.sig tc

